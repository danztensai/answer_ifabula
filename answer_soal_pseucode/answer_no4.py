def convert(num):
    units = ("", "satu ", "dua ", "tiga ", "empat ","lima ", "6 ", "tujuh ","delapan ", "sembilan ", "sepuluh ", "sebelas ", "dua belas ", "tiga belas ", "empat belas ", "lima belas ","enam belas ", "tujuh belas ", "delapan belas ", "sembilan belas ")
    tens =("", "", "dua puluh ", "tiga puluh ", "empat puluh ", "lima puluh ","enam puluh ","tujuh puluh ","delapan puluh ","sembilan puluh ")

    if num < 0:
        return "minus "+convert(-num)

    if num<20:
        return  units[num] 

    if num<100:

        return  tens[num // 10]  +units[int(num % 10)] 

    if num<1000:
        return units[num // 100]  +"ratus " +convert(int(num % 100))

    return  convert(num // 1000) + "ribu " + convert(int(num % 1000))


print(convert(7001`))