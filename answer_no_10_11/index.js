const express = require('express');
const app = express();
 
app.get('/', (req, res) => {
	let userId = req.header('User-id');
	let scope = req.header('Scope');
	if(userId != 'ifabula' && scope != 'user')
	{
		res.status(401).json({responseCode: 401,responseMessage: "UNAUTHORIZED"});
	}else{
		res.send('Hello World');
	}
    
});

app.post('/', function (req, res) {
	let userId = req.header('User-id');
	let scope = req.header('Scope');
	if(userId != 'User-id' && scope != 'user')
	{
		res.status(401).json({responseCode: 401,responseMessage: "UNAUTHORIZED"});
	}else{
		res.send('Hello World From Post Request');
	}
})


app.listen(3000, () => console.log('Open Your Browser and point to http://localhost:3000'));